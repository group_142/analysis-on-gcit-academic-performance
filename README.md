## GCIT 3rd Year Result Analysis


## Aim
To analyze the academic performance of GCIT 3rd students.


## Objective of the project:
To find out the overall performance pattern of GCIT students.
To create a proper platform(i.e dashboard) that provides easily accessible information related to academic results.


## Problem statement:
Academic success is important because it directly decides the positive outcomes of the students after graduating. It is important for the successful development of young people in society.Students who do well in school are able to make a better transition into adulthood which helps to achieve occupational and economic success. So far there is no system that can analyze the students past performance and help them in predicting how they are going to perform in the future. It is important for students to know in what field they are lacking. For example, in the case of college students, if they have performed low in this semester they have to find out the reason behind their low academic performance so that they can perform better in the future. In the current scenario, most of the students can’t figure out why their performance is getting low or changing in each semester. To help those students in improving their academic performance by analyzing their current result and situation, we are going to develop a system that nalyzes the academic performance of GCIT students.


### Requirements

## Software requirement
Jupyter Notebook for loading data and for the data visualization.
Ploty Dash for the creating the dashboard.
Heroku for deployment of the dashboard.


## Hardware requirements
Laptop with i5 and above
16Gb RAM recommended.

## System Design
This system includes three different stages: Data Preparation, Model Development, Product Deployment.
The Data preparation layer is related to the management of the data required for the system.In this layer it includes several processes like data abstraction, feature abstraction, data cleaning, analysis data and data visualization. This layer will make sure that the data collected is related to the system and there is no redundant data.
Model Development layer is the stage where the coding phase of the system will be progressed. This layer includes code debug, erify code and analysis of final code.
The model development layer will ensure that the system designed is bug free.The complete system developed by a team will be displayed at the production deploy layer.This layer will help the team to identify whether the system developed satisfies the aim and goal of the project.


### Output:
## Dashboard link
https://miniprj-group14.herokuapp.com/
